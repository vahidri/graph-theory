# Graph Theory Project, Fall 2018
#### Licensed under GPL 2.0 to The Graph Group of "Boss, the G"

For each implementation, there are two files, 'run.sh' which should be run to get an output, and 'test.txt' which is the test given for running the algorithm on.

Also, there is already an output in each directory, in the form of *png* files, which are the output images of the codes.

Algorithms:
* [Hopcroft-Karp Algorithm: finding a Maximum Matching in Bipartite Graphs](https://en.wikipedia.org/wiki/Hopcroft%E2%80%93Karp_algorithm)
* [Kruskal's Algorithm: finding an MST](https://en.wikipedia.org/wiki/Kruskal%27s_algorithm)
* [Prim's Algorithm: finding an MST](https://en.wikipedia.org/wiki/Prim%27s_algorithm)

